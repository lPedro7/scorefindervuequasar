import Vue from 'vue'
import axios from 'axios'
import { Notify } from 'quasar'
import router from 'src/router'
import GAuth from 'vue-google-oauth2'
Vue.prototype.$axios = axios;


const options = {
    clientId: "59668076614-icj2iqpani04tvkvk5met0aaif4tptg1.apps.googleusercontent.com"
  };
  
  Vue.use(GAuth, options)
export default async ({ Vue, router }) => {
    Vue.prototype.$axios = axios;

    axios.interceptors.request.use(
        function (config) {

            config.withCredentials = true;
            config.headers.common["Authorization"] =localStorage.getItem('token')
            config.headers.common["Content-Type"] = "application/json";
            return config;
        },
        function (error) {
            return Promise.reject(error)
        }
    )

    axios.interceptors.response.use(
        function (response) {
            if (response.data.notifyMessage) createNotification("positive", response.data.notifyMessage)
            return response
        },
        function(error){
            return "ERROR"
        }
    )

    function createNotification(type, msg) {
        Notify.create({
            type: type,
            message: msg
        })
    }


};
